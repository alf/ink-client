// More info on Webpack's Node API here: https://webpack.github.io/docs/node.js-api.html
// Allowing console calls below since this is a build file.
/*eslint-disable no-console */

import { DEMO_DIST_DIR } from './build-constants';
import { runWebpackWithEnv } from './buildShared';

process.env.NODE_ENV = 'production'; // this assures React is built in demo ("prod") mode and that the Babel dev config doesn't apply.
process.env.STAGE = 'demo';

runWebpackWithEnv(DEMO_DIST_DIR);