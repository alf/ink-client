import webpack from 'webpack';
import webpackConfigBuilder from '../webpack.config';
import colors from 'colors';
import { argv as args } from 'yargs';

export function runWebpackWithEnv(dir) {
    const webpackConfig = webpackConfigBuilder(process.env.NODE_ENV);

    webpack(webpackConfig).run((err, stats) => {
        const inSilentMode = args.s; // set to true when -s is passed on the command

        if (!inSilentMode) {
            console.log(`Generating minified bundle for ${process.env.NODE_ENV} use for stage ${process.env.STAGE} via Webpack...`.bold.blue);
        }

        if (err) { // so a fatal error occurred. Stop here.
            console.log(err.bold.red);

            return 1;
        }

        const jsonStats = stats.toJson();

        if (jsonStats.hasErrors) {
            return jsonStats.errors.map(error => console.log(error.red));
        }

        if (jsonStats.hasWarnings && !inSilentMode) {
            console.log('Webpack generated the following warnings: '.bold.yellow);
            jsonStats.warnings.map(warning => console.log(warning.yellow));
        }

        if (!inSilentMode) {
            console.log(`Webpack stats: ${stats}`);
        }

        // if we got this far, the build succeeded.
        console.log(`Your app has been compiled in production mode for env ${process.env.NODE_ENV} for stage ${process.env.STAGE} and written to ${dir}.`.green.bold);

        return 0;
    });
}