// This file configures a web server for testing the production build
// on your local machine.

import browserSync from 'browser-sync';
import historyApiFallback from 'connect-history-api-fallback';
import { PRODUCTION_DIST_DIR } from './build-constants';

// Run Browsersync
browserSync({
  port: 3000,
  ui: {
    port: 3001
  },
  server: {
    baseDir: PRODUCTION_DIST_DIR
  },

  files: [
    'src/*.html'
  ],

  middleware: [historyApiFallback()]
});
