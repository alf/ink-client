import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import appState from './reducer';

const rootReducer = combineReducers({
  form: formReducer,
  appState
});

export default rootReducer;
