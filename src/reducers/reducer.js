import * as pusherConstants from '../constants/PusherActions';
import * as actionTypes from '../constants/ActionTypes';
import * as alertTypes from '../constants/AlertTypes';
import * as viewConstants from '../constants/Views';
import _ from 'lodash';

import objectAssign from 'object-assign';

const previousState =
	JSON.parse(localStorage.getItem('ink-client-session')) || {
		authToken: null,
		isLoading: false,
		signedIn: false,
		account: null
	};

export const initialState = {
	downloadPath: null,
  downloadLoading: false,
	recipes: null,
	alerts: [],
	session: previousState,
	authInProgress: false,
	getRecipesInProgress: false,
	stepClassList: [],
	availableStepClassList: [],
	executionPlaceholderCount: 0,
	admin: {accounts: null, getStatusReportInProgress: false},
	currentShowRecipeView: viewConstants.SHOW_RECIPE_STEP_VIEW
};

function augmentRecipe(recipe) {
	recipe.process_chains.map(process_chain => {
		process_chain.input_file_list_collapse = true;
		process_chain.output_file_list_collapse = true;
		process_chain.process_steps.map(process_step => {
			process_step.file_list_collapse = true;
			process_step.savingPreset = false;
		});
	});
	recipe.recipe_steps.map(recipe_step => {
		recipe_step.working_execution_parameters = [];
		recipe_step.parameterView = viewConstants.SHOW_PARAMETER_FORM;
		recipe_step.isLoadingPresets = false;
		Object.keys(recipe_step.execution_parameters).map(function(key, idx) {
			recipe_step.working_execution_parameters.push(Object.assign({}, {key: key, value: recipe_step.execution_parameters[key], editing: false, error: ""}));
		});
	});
	return recipe;
}

export default function appState(state = initialState, action) {
	switch (action.type) {
		case actionTypes.SET_ALERT:
		{
			let newState = objectAssign({}, state);

			newState.alerts = [];

			let alertType = action.alertType || "error";

			_.map(_.flatten([action.message]), function(alert) {
				let newHash = {};
				newHash["content"] = alert;
				newHash["id"] = Date.now();
				newHash["type"] = alertType;
				newState.alerts.push(newHash);
			});

			return newState;
		}

		/////////////////////////// auth ////////////////////////////

		case actionTypes.SIGN_IN_REQUEST:
		{
      let newState = objectAssign({}, state);

      newState.authInProgress =  true;

      return newState;
    }

		case actionTypes.SIGN_IN_SUCCESS:
		{

      let newState = objectAssign({}, state);
			let account = action.account;

			newState.authInProgress= false;

			newState.session.signedIn = true;
			newState.session.account = {
					email: account.email,
					name: account.name,
					uid: account.uid,
					id: account.id,
					admin: account.admin
				};

      return newState;
    }

		case actionTypes.SIGN_IN_FAILURE:
    {
      let newState = objectAssign({}, state);
			newState.authInProgress= false;

			newState.session = {
				signedIn: false,
				account: null
			};
			console.log("sign in failed:", action.error)
      return newState;
    }

		case actionTypes.RESET_AUTH:
		{
			let newState = objectAssign({}, state);

			newState.session = {
				account: null,
				authToken: null,
				expiry: null,
				tokenType: null,
				client: null,
				uid: null
			};

			localStorage.setItem('ink-client-session', JSON.stringify(newState.session));

			return newState;
		}

		case actionTypes.UPDATE_AUTH_HEADERS:
		{
				let newState = objectAssign({}, state);

				newState.session.authToken = action.authToken;
				newState.session.expiry = action.expiry;
				newState.session.tokenType = action.tokenType;
				newState.session.client = action.client;
				newState.session.uid = action.uid;

				localStorage.setItem('ink-client-session', JSON.stringify(newState.session));

				return newState;
		}
		case actionTypes.SIGN_OUT_SUCCESS:
		{
			let newState = objectAssign({}, initialState);

			return newState;
		}
		case actionTypes.SIGN_OUT_FAILURE:
		{
			console.log("sign out failed:", action.error)
			return initialState;
		}
    case actionTypes.SIGN_UP_SUCCESS:
    {
      let newState = objectAssign({}, state);

      return newState;
    }
    case actionTypes.SIGN_UP_FAILURE:
    {
      let newState = objectAssign({}, state);

      newState.session.account = {};
			console.log("sign up failed:", action.error)

      return newState;
    }

		/////////////////////////// admin ////////////////////////////////

		case actionTypes.GET_ALL_ACCOUNTS_REQUEST:
		{
			let newState = objectAssign({}, state);

			newState.admin.getAccountsInProgress = true;

			return newState;
		}

		case actionTypes.GET_ALL_ACCOUNTS_SUCCESS:
		{
			let newState = objectAssign({}, state);
			newState.admin.accounts = action.accounts;
			newState.admin.getAccountsInProgress = false;
			return newState;
		}

		case actionTypes.GET_ALL_ACCOUNTS_FAILURE:
		{
			let newState = objectAssign({}, state);

			newState.admin.getAccountsInProgress = false;
			console.log("get all accounts failed:", action.error)

			return newState;
		}

		case actionTypes.GET_STATUS_REPORT_REQUEST:
		{
			let newState = objectAssign({}, state);

			newState.admin.getStatusReportInProgress = true;

			return newState;
		}

		case actionTypes.GET_STATUS_REPORT_SUCCESS:
		{
			let newState = objectAssign({}, state);
			newState.admin.statusReport = action.statusReport;
			newState.admin.getStatusReportInProgress = false;
			return newState;
		}

		case actionTypes.GET_STATUS_REPORT_FAILURE:
		{
			let newState = objectAssign({}, state);

			newState.admin.getStatusReportInProgress = false;
			console.log("status report failed:", action.error)

			return newState;
		}

		case actionTypes.AVAILABLE_GEM_LIST_REQUEST:
		{
			let newState = objectAssign({}, state);

			newState.admin.getAvailableStepGemsInProgress = true;

			return newState;
		}

		case actionTypes.AVAILABLE_GEM_LIST_SUCCESS:
		{
			let newState = objectAssign({}, state);
			newState.admin.availableStepGems = action.available_step_gems;
			newState.admin.getAvailableStepGemsInProgress = false;
			return newState;
		}

		case actionTypes.AVAILABLE_GEM_LIST_FAILURE:
		{
			let newState = objectAssign({}, state);

			newState.admin.getAvailableStepGemsInProgress = false;
			console.log("available gem list failed:", action.error)

			return newState;
		}

		///////////////////////////////// get recipes ///////////////////////////

		case actionTypes.GET_ALL_RECIPES_REQUEST:
		{
			let newState = objectAssign({}, state);

			newState.getRecipesInProgress = true;

      return newState;
		}

    case actionTypes.GET_ALL_RECIPES_SUCCESS:
    {
      let newState = objectAssign({}, state);

			newState.getRecipesInProgress = false;
			newState.recipes = action.data.recipes;

      return newState;
    }

    case actionTypes.GET_ALL_RECIPES_FAILURE:
    {
      let newState = objectAssign({}, state);

			newState.recipes = [];
			newState.getRecipesInProgress = false;
			console.log("get all recipes failed:", action.error)
      return newState;
    }

		case actionTypes.GET_FULL_RECIPES_REQUEST:
		{
			let newState = objectAssign({}, state);

			newState.getRecipesInProgress = true;

			return newState;
		}

		case actionTypes.GET_FULL_RECIPES_SUCCESS:
		{
			let newState = objectAssign({}, state);

			newState.getRecipesInProgress = false;
			newState.recipes = action.data.recipes;
			newState.recipes.map(recipe => {
				recipe = augmentRecipe(recipe);
			});

			return newState;
		}

		case actionTypes.GET_FULL_RECIPES_FAILURE:
		{
			let newState = objectAssign({}, state);

			newState.recipes = [];
			newState.getRecipesInProgress = false;
			console.log("get full recipes failed:", action.error)
			return newState;
		}

		case actionTypes.SELECT_RECIPE:
		{
			let newState = objectAssign({}, state);

			newState.selectedRecipeID = action.selectedRecipeID;
			newState.selectedRecipe = _.find(state.recipes,
      _.matchesProperty('id', _.toNumber(action.selectedRecipeID)));

			return newState;
		}

		case actionTypes.DESELECT_RECIPE:
		{
			let newState = objectAssign({}, state);

			newState.selectedRecipeID = null;
			newState.selectedRecipe = null;

			return newState;
		}

		case actionTypes.GET_RECIPE_REQUEST:
		{
			let newState = objectAssign({}, state);

			newState.getRecipeInProgress = true;

			return newState;
		}

		case actionTypes.GET_RECIPE_SUCCESS:
		{
			let newState = objectAssign({}, state);

			newState.getRecipeInProgress = false;
			// newState.selectedRecipe = action.recipe;
			// newState.selectedRecipeID = action.recipe.id;
			let theRecipe = action.recipe.recipe;
			if(_.isNull(newState.recipes)) {
				newState.recipes = [];
				newState.recipes.push(augmentRecipe(theRecipe));
			}
			else {
				// let recipe = _.find(newState.recipes, _.matchesProperty('id', action.recipe.id));
				var index = _.findIndex(newState.recipes, _.matchesProperty('id', theRecipe.id));
				newState.recipes.splice(index, 1, augmentRecipe(theRecipe));
			}

			// newState.selectedRecipe.process_chains.map(process_chain => {
			// 	process_chain.input_file_list_collapse = true;
			// 	process_chain.output_file_list_collapse = true;
			// 	process_chain.process_steps.map(process_step => {
			// 		process_step.file_list_collapse = true;
			// 	});
			// });
			// newState.selectedRecipe.recipe_steps.map(recipe_step => {
			// 	recipe_step.working_execution_parameters = Object.assign({}, recipe_step.execution_parameters);
			// 	recipe_step.edit_keys = [];
			// });

			return newState;
		}

		case actionTypes.GET_RECIPE_FAILURE:
		{
			let newState = objectAssign({}, state);
			console.log("get recipe failed:", action.error)
			return newState;
		}

		////////////////////// recipe execution //////////////////////////////////

		case actionTypes.EXECUTE_RECIPE_REQUEST:
		{
			let newState = objectAssign({}, state);

			newState.alerts = [];
			newState.executionPlaceholderCount += 1;

			return newState;
		}

		case actionTypes.EXECUTE_RECIPE_SUCCESS:
		{
			let newState = objectAssign({}, state);

			newState.executionPlaceholderCount -= 1;

			return newState;
		}

		case actionTypes.EXECUTE_RECIPE_FAILURE:
		{
			let newState = objectAssign({}, state);

			newState.executionPlaceholderCount -= 1;
			console.log("execute recipe failed:", action.error)

			return newState;
		}

		////////////////////// recipe favourites //////////////////////////////////

		case actionTypes.SET_RECIPE_FAVOURITE:
		{
			let newState = objectAssign({}, state);

			let recipe = _.find(newState.recipes, _.matchesProperty('id', action.recipeId));
			recipe.favourite = action.favourite;
			recipe.pendingFavourite = false;

			return newState;
		}

		case actionTypes.FAVOURITE_RECIPE_REQUEST:
		{
			let newState = objectAssign({}, state);

			newState.alerts = [];
			let recipe = _.find(newState.recipes, _.matchesProperty('id', action.recipeId));
			recipe.pendingFavourite = true;

			return newState;
		}

		case actionTypes.FAVOURITE_RECIPE_FAILURE:
		{
			let newState = objectAssign({}, state);

			let recipe = _.find(newState.recipes, _.matchesProperty('id', action.recipeId));
			recipe.pendingFavourite = false;
			console.log("favourite recipe failed:", action.error)

			return newState;
		}

		case actionTypes.UNFAVOURITE_RECIPE_REQUEST:
		{
			let newState = objectAssign({}, state);

			newState.alerts = [];
			let recipe = _.find(newState.recipes, _.matchesProperty('id', action.recipeId));
			recipe.pendingFavourite = true;

			return newState;
		}

		case actionTypes.UNFAVOURITE_RECIPE_FAILURE:
		{
			let newState = objectAssign({}, state);

			let recipe = _.find(newState.recipes, _.matchesProperty('id', action.recipeId));
			recipe.pendingFavourite = false;
			console.log("unfavourite recipe failed:", action.error)

			return newState;
		}

		////////////////////////// presets ////////////////////////////////////

		case actionTypes.GET_PRESETS_FOR_RECIPE_STEP_REQUEST:
		{
			let newState = objectAssign({}, state);

			let recipe = _.find(newState.recipes, _.matchesProperty('id', action.recipeId));
			let recipeStep = _.find(recipe.recipe_steps, _.matchesProperty('id', action.recipeStepId));

			recipeStep.isLoadingPresets = true;
			recipeStep.presets = [];

			return newState;
		}

		case actionTypes.GET_PRESETS_FOR_RECIPE_STEP_SUCCESS:
		{
			let newState = objectAssign({}, state);

			let recipe = _.find(newState.recipes, _.matchesProperty('id', action.recipeId));
			let recipeStep = _.find(recipe.recipe_steps, _.matchesProperty('id', action.recipeStepId));

			recipeStep.isLoadingPresets = false;
			recipeStep.presets = action.json;

			return newState;
		}

		case actionTypes.GET_PRESETS_FOR_RECIPE_STEP_FAILURE:
		{
			let newState = objectAssign({}, state);

			let recipe = _.find(newState.recipes, _.matchesProperty('id', action.recipeId));
			let recipeStep = _.find(recipe.recipe_steps, _.matchesProperty('id', action.recipeStepId));

			recipeStep.isLoadingPresets = false;
			recipeStep.presets = [];

			console.log("get presets for recipe step failed:", action.error)
			return newState;
		}

		case actionTypes.SET_PRESET_FOR_RECIPE_STEP_EXECUTION:
		{
			let newState = objectAssign({}, state);

			let recipe = _.find(newState.recipes, _.matchesProperty('id', action.recipeId));
			let recipeStep = _.find(recipe.recipe_steps, _.matchesProperty('id', action.recipeStepId));

			recipeStep.selectedPreset = action.preset;

			return newState;
		}

		case actionTypes.CREATE_PRESET_REQUEST:
		{
			let newState = objectAssign({}, state);

			let recipe = _.find(newState.recipes, _.matchesProperty('id', action.recipeId));
			let processChain = _.find(recipe.process_chains, _.matchesProperty('id', action.processStep.process_chain_id));
			let processStep = _.find(processChain.process_steps, _.matchesProperty('id', action.processStep.id));

			processStep.savingPreset = true;

			return newState;
		}

		case actionTypes.CREATE_PRESET_SUCCESS:
		{
			let newState = objectAssign({}, state);

			let recipe = _.find(newState.recipes, _.matchesProperty('id', action.recipeId));
			let processChain = _.find(recipe.process_chains, _.matchesProperty('id', action.processStep.process_chain_id));
			let processStep = _.find(processChain.process_steps, _.matchesProperty('id', action.processStep.id));

			let recipeStep = _.find(recipe.recipe_steps, _.matchesProperty('position', action.processStep.position));
			recipeStep.recipe_step_presets.push(action.preset);
			processStep.savingPreset = false;
			processStep.presetSaved = true;

			return newState;
		}

		case actionTypes.CREATE_PRESET_FAILURE:
		{
			let newState = objectAssign({}, state);

			let recipe = _.find(newState.recipes, _.matchesProperty('id', action.recipeId));
			let processChain = _.find(recipe.process_chains, _.matchesProperty('id', action.processStep.process_chain_id));
			let processStep = _.find(processChain.process_steps, _.matchesProperty('id', action.processStep.id));

			processStep.savingPreset = false;
			console.log("saving preset failed:", action.error)

			return newState;
		}

		////////////////////// create recipe //////////////////////////////////

		case actionTypes.CREATE_RECIPE_REQUEST:
		{
			let newState = objectAssign({}, state);

			newState.alerts = [];

			return newState;
		}

		case actionTypes.CREATE_RECIPE_SUCCESS:
		{
			let newState = objectAssign({}, state);

			newState.selectedRecipe = action.recipe;
			newState.selectedRecipeID = action.recipe.id;

			return newState;
		}

		case actionTypes.CREATE_RECIPE_FAILURE:
		{
			let newState = objectAssign({}, state);
			console.log("create recipe failed:", action.error)

			return newState;
		}

		/////////////////////////// edit recipe //////////////////////////////

		case actionTypes.EDIT_RECIPE_REQUEST:
		{
			let newState = objectAssign({}, state);

			newState.alerts = [];

			return newState;
		}

		case actionTypes.EDIT_RECIPE_SUCCESS:
		{
			let newState = objectAssign({}, state);

			return newState;
		}

		case actionTypes.EDIT_RECIPE_FAILURE:
		{
			let newState = objectAssign({}, state);
			console.log("create recipe failed:", action.error)
			return newState;
		}

		case actionTypes.UPDATE_RECIPE_IN_LIST:
		{
			let newState = objectAssign({}, state);

			let recipe = action.recipe;
			let recipes = newState.recipes;
			if(_.isEmpty(newState.recipes)) {
				return newState;
			}
			let oldRecipe = _.find(recipes, _.matchesProperty('id', action.recipe.id));

			if(!_.isEmpty(oldRecipe)) {
				_.remove(newState.recipes, {
					id: oldRecipe.id
				});
			}

			newState.recipes.push(recipe);
			return newState;
		}

		case actionTypes.REMOVE_RECIPE_FROM_LIST:
		{
			let newState = objectAssign({}, state);

			let recipe = action.recipe;
			let recipes = newState.recipes;
			if(_.isEmpty(newState.recipes)) {
				return newState;
			}
			let theRecipe = _.find(recipes, _.matchesProperty('id', action.recipe.id));

			if(!_.isEmpty(theRecipe)) {
				_.remove(newState.recipes, {
					id: theRecipe.id
				});
			}

			return newState;
		}

		case actionTypes.UPDATE_SELECTED_RECIPE:
		{
			let newState = objectAssign({}, state);

			newState.selectedRecipe = action.recipe;

			return newState;
		}

		////////////////////////////// downloading things /////////////////////////////////

		case actionTypes.DOWNLOAD_FILE_REQUEST:
		{
			let newState = objectAssign({}, state);

			newState.alerts = [];

			return newState;
		}

		case actionTypes.DOWNLOAD_FILE_SUCCESS:
		{
			let newState = objectAssign({}, state);

			return newState;
		}

		case actionTypes.DOWNLOAD_FILE_FAILURE:
		{
			let newState = objectAssign({}, state);
			console.log("download file failed:", action.error)

			return newState;
		}

		//////////////////////////// creating new recipes //////////////////////////////

		case actionTypes.RESET_STEP_LIST:
		{
			let newState = objectAssign({}, state);

			newState.stepClassList = [];

			return newState;
		}

		case actionTypes.ADD_TO_STEP_LIST:
		{
			let newState = objectAssign({}, state);

			newState.stepClassList.push(action.stepClassName);

			return newState;
		}

		case actionTypes.REMOVE_FROM_STEP_LIST:
		{
			let newState = objectAssign({}, state);

			newState.stepClassList.splice(action.index, 1);

			return newState;
		}

		case actionTypes.RESET_PLACEHOLDERS:
		{
			let newState = objectAssign({}, state);

			newState.executionPlaceholderCount = 0;

			return newState;
		}

		//////////////////////////// delete recipe //////////////////////////////

		case actionTypes.DELETE_RECIPE_REQUEST:
		{
			let newState = objectAssign({}, state);

			newState.alerts = [];

			return newState;
		}

		case actionTypes.DELETE_RECIPE_SUCCESS:
		{
			let newState = objectAssign({}, state);

			return newState;
		}

		case actionTypes.DELETE_RECIPE_FAILURE:
		{
			let newState = objectAssign({}, state);
			console.log("delete recipe failed:", action.error)

			return newState;
		}

		/////////////////////////////// pusher/slanger stuff /////////////////////////////////

		case pusherConstants.CHAIN_CREATED:
		{
			let newState = objectAssign({}, state);

			let theRecipe = _.find(newState.recipes, _.matchesProperty('id', action.data.recipe_id));
			let newChain = action.data;

			theRecipe.process_chains.unshift(newChain);
			if(newState.selectedRecipe && newState.selectedRecipe.id === theRecipe.id) {
				newState.selectedRecipe.process_chains.unshift(newChain);
			}

			return newState;
		}

		case actionTypes.MARK_CHAIN_AS_STARTED:
		{
			let newState = objectAssign({}, state);

			let theRecipe = _.find(newState.recipes, _.matchesProperty('id', action.data.recipe_id));
			let theChain = _.find(theRecipe.process_chains, _.matchesProperty('id', action.data.chain_id));

			theChain.executed_at = Date.now();

			return newState;
		}

		case pusherConstants.MARK_CHAIN_AS_COMPLETED:
		{
			let newState = objectAssign({}, state);

			let theRecipe = _.find(newState.recipes, _.matchesProperty('id', action.data.recipe_id));
			let theChain = _.find(theRecipe.process_chains, _.matchesProperty('id', action.data.chain_id));

			theChain.finished_at = Date.now();
			theChain.output_file_manifest = action.data.output_file_manifest;
			theChain.output_file_list_collapse = true;

			return newState;
		}

		case pusherConstants.MARK_CHAIN_AS_ERRORED:
		{
			let newState = objectAssign({}, state);

			let theRecipe = _.find(newState.recipes, _.matchesProperty('id', action.data.recipe_id));
			let theChain = _.find(theRecipe.process_chains, _.matchesProperty('id', action.data.chain_id));

			theChain.successful = false;
			theChain.finished_at = Date.now();

			return newState;
		}

		case pusherConstants.MARK_STEP_AS_STARTED:
		{
			let newState = objectAssign({}, state);

			let theRecipe = _.find(newState.recipes, _.matchesProperty('id', action.data.recipe_id));
			let theChain = _.find(theRecipe.process_chains, _.matchesProperty('id', action.data.chain_id));
			let theStep = _.find(theChain.process_steps, _.matchesProperty('position', action.data.position));

			theStep.started_at = Date.now();
			theStep.in_progress = true;
			theStep.file_list_collapse = true;

			if(_.isNil(theChain.executed_at)) {
				theChain.executed_at = Date.now();
			}

			theStep.version = action.data.version;
			return newState;
		}

		case pusherConstants.MARK_STEP_AS_COMPLETED:
		{
			let newState = objectAssign({}, state);

			let theRecipe = _.find(newState.recipes, _.matchesProperty('id', action.data.recipe_id));
			let theChain = _.find(theRecipe.process_chains, _.matchesProperty('id', action.data.chain_id));
			let theStep = _.find(theChain.process_steps, _.matchesProperty('position', action.data.position));

			theStep.finished_at = Date.now();
			theStep.output_file_manifest = action.data.output_file_manifest;
			theStep.successful = action.data.successful;
			theStep.notes = action.data.notes;
			theStep.execution_errors = action.data.execution_errors;
			theStep.in_progress = false;
			theStep.file_list_collapse = true;
			theStep.process_log_location = action.data.process_log_location;

			return newState;
		}

		//////////////////////////////// available step list //////////////////////////////////

		case pusherConstants.AVAILABLE_STEP_LIST_REQUEST:
		{
			let newState = objectAssign({}, state);

			newState.getStepListInProgress = true;
			newState.availableStepClassList = [];

			return newState;
		}

		case actionTypes.AVAILABLE_STEP_LIST_SUCCESS:
		{
			let newState = objectAssign({}, state);

			newState.getStepListInProgress = false;
			newState.availableStepClassList = action.available_step_classes;

			return newState;
		}

		case actionTypes.AVAILABLE_STEP_LIST_FAILURE:
		{
			let newState = objectAssign({}, state);

			newState.getStepListInProgress = false;
			console.log("available step list failed:", action.error)

			return newState;
		}

/////////////////////////// expanding and collapsing file lists in the UI ///////////////////////////////


		case actionTypes.COLLAPSE_CHAIN_INPUT_FILE_LIST:
		{
			let newState = objectAssign({}, state);

			let theRecipe = _.find(newState.recipes, _.matchesProperty('id', action.recipeId));
			let theChain = _.find(theRecipe.process_chains, _.matchesProperty('id', action.chainId));

			theChain.input_file_list_collapse = true;

			return newState;
		}

		case actionTypes.EXPAND_CHAIN_INPUT_FILE_LIST:
		{
			let newState = objectAssign({}, state);

			let theRecipe = _.find(newState.recipes, _.matchesProperty('id', action.recipeId));
			let theChain = _.find(theRecipe.process_chains, _.matchesProperty('id', action.chainId));

			theChain.input_file_list_collapse = false;

			return newState;
		}

		case actionTypes.COLLAPSE_CHAIN_OUTPUT_FILE_LIST:
		{
			let newState = objectAssign({}, state);

			let theRecipe = _.find(newState.recipes, _.matchesProperty('id', action.recipeId));
			let theChain = _.find(theRecipe.process_chains, _.matchesProperty('id', action.chainId));

			theChain.output_file_list_collapse = true;

			return newState;
		}

		case actionTypes.EXPAND_CHAIN_OUTPUT_FILE_LIST:
		{
			let newState = objectAssign({}, state);

			let theRecipe = _.find(newState.recipes, _.matchesProperty('id', action.recipeId));
			let theChain = _.find(theRecipe.process_chains, _.matchesProperty('id', action.chainId));

			theChain.output_file_list_collapse = false;

			return newState;
		}

		case actionTypes.COLLAPSE_STEP_FILE_LIST:
		{
			let newState = objectAssign({}, state);

			let theRecipe = _.find(newState.recipes, _.matchesProperty('id', action.recipeId));
			let theChain = _.find(theRecipe.process_chains, _.matchesProperty('id', action.chainId));
			let theStep = _.find(theChain.process_steps, _.matchesProperty('id', action.stepId));

			theStep.file_list_collapse = true;

			return newState;
		}

		case actionTypes.EXPAND_STEP_FILE_LIST:
		{
			let newState = objectAssign({}, state);

			let theRecipe = _.find(newState.recipes, _.matchesProperty('id', action.recipeId));
			let theChain = _.find(theRecipe.process_chains, _.matchesProperty('id', action.chainId));
			let theStep = _.find(theChain.process_steps, _.matchesProperty('id', action.stepId));

			theStep.file_list_collapse = false;

			return newState;
		}

		case actionTypes.SET_VIEW:
		{
			let newState = objectAssign({}, state);

			newState.currentShowRecipeView = action.viewName;

			return newState;
		}

		case actionTypes.SET_PARAMETER_TAB_VIEW:
		{
			let newState = objectAssign({}, state);

			let theRecipe = _.find(newState.recipes, _.matchesProperty('id', action.recipeStep.recipe_id));
			let theStep = _.find(theRecipe.recipe_steps, _.matchesProperty('id', action.recipeStep.id));
			theStep.parameterView = action.viewName;

			return newState;
		}

		default:
			return state;
	}
}
