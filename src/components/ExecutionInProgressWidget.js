import React, {PropTypes} from 'react';
import _ from 'lodash';

function isRecent(startedAt) {
  let time = _.now() - new Date(startedAt).valueOf()
  return(time < 86400000);
}

export function processChainsInProgress(recipe) {
  let isProcessing = false;
  _.forEach(recipe.process_chains, function(chain) {
    if(_.isNil(chain.finished_at) && isRecent(chain.created_at)) {
      isProcessing = true;
    }
  });
  return(isProcessing);
}

const ExecutionInProgressWidget = (props) => {

  let inProgress = processChainsInProgress(props.recipe);

  if (inProgress) {
    return(
      <span className="fa fa-gear fa-spin fa-fw"/>
    );
  }
  else {
    return(
      <span className="fa fa-gear grey"/>
    );
  }
};

ExecutionInProgressWidget.propTypes = {
  recipe: PropTypes.object.isRequired
};

export default ExecutionInProgressWidget;
