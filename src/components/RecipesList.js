import React, { PropTypes } from 'react';
import RecipeListItem from './RecipeListItem.js';
import _ from 'lodash';
import { Link } from 'react-router';

const RecipesList = (props) => {
  if(_.isEmpty(props.session)) {
    return(
      <p className="help-block disabled">Sign in to see your recipes</p>
    );
  }

  if(props.getRecipesInProgress === true) {
    return(
      <div className="loading-centered">
        <div><span className="fa fa-cog fa-spin fa-3x fa-fw" /></div>
        <div><span className="small-info">loading...</span></div>
      </div>
    );
  }
  else if(_.isEmpty(props.recipes)) {
    return(
      <div>
        <h4>You have not have any recipes yet!</h4>
        <div>
          <p>Why not <Link to="/recipes/new">create a new one</Link>?</p>
        </div>
      </div>
    );
  }

  return (
    <div>
      <ul className="recipe-list-container">
        { props.recipes.map(recipe =>
          <RecipeListItem key={recipe.id}
          recipe={recipe}
          isSelected={recipe.id == props.selectedRecipeID} />)
        }
      </ul>
    </div>
  );
};

RecipesList.propTypes = {
  session: PropTypes.object,
  getRecipesInProgress: PropTypes.bool.isRequired,
  recipes: PropTypes.array.isRequired,
  selectedRecipeID: PropTypes.number
};

export default RecipesList;
