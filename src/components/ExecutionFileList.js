import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _  from "lodash";
import SmoothCollapse from 'react-smooth-collapse';

import * as actions from '../actions/authenticationActions';
import { downloadInputFile, downloadOutputFile, downloadInputZip, downloadChainOutputZip, downloadChainOutputFile } from '../businessLogic/fileLogic';
import { collapseChainInputFileList, expandChainInputFileList, collapseChainOutputFileList, expandChainOutputFileList } from '../actions/recipeActions';

export class ExecutionFileList extends Component {

  handleChainOutputZipDownload = (e, processChainId) => {
    e.preventDefault();
    const { appState, dispatch } = this.props;
    dispatch(downloadChainOutputZip(processChainId, appState, dispatch));
  }

  handleChainOutputFileDownload = (e, filePath, processChainId) => {
    e.preventDefault();
    const { appState, dispatch } = this.props;
    dispatch(downloadChainOutputFile(filePath, processChainId, appState, dispatch));
  }

  handleInputZipDownload = (e, processChainId) => {
    e.preventDefault();
    const { appState, dispatch } = this.props;
    dispatch(downloadInputZip(processChainId, appState, dispatch));
  }

  handleInputFileDownload = (e, filePath, processChainId) => {
    e.preventDefault();
    const { appState, dispatch } = this.props;
    dispatch(downloadInputFile(filePath, processChainId, appState, dispatch));
  }

  handleToggle = (e, isInput, toExpand, chainId, recipeId) => {
    e.preventDefault();
    const { appState, dispatch } = this.props;
    if(isInput == true && toExpand == true) {
      dispatch(expandChainInputFileList(chainId, recipeId, appState));
    } else if (isInput == true) {
      dispatch(collapseChainInputFileList(chainId, recipeId, appState));
    } else if (toExpand == true) {
      dispatch(expandChainOutputFileList(chainId, recipeId, appState));
    } else {
      dispatch(collapseChainOutputFileList(chainId, recipeId, appState));
    }
  }

  renderFileLink(filePath, isOutput, id) {
    if(_.isNil(filePath) || _.isEmpty(filePath)) {
      return(
        <span className="small-info-text">[ no files ]</span>
      );
    }
    else if(isOutput === true) {
      return(
        <a href={`#id=${id}`} onClick={e => this.handleChainOutputFileDownload(e, filePath, id)}>{filePath} <span className="fa fa-download"/></a>
      );
    }
    else if(isOutput === false) {
      return(
        <a href={`#id=${id}`} onClick={e => this.handleInputFileDownload(e, filePath, id)}>{filePath} <span className="fa fa-download"/></a>
      );
    }
    else {
      return(<span className="small-info-text">[ no files ]</span>);
    }
  }

  renderChainZipLink(isOutput, id) {
    if(isOutput === true) {
      return(
        <a href={`#id=${id}`} onClick={e => this.handleChainOutputZipDownload(e, id)}>all files (zip) <span className="fa fa-download"/></a>
      );
    }
    else if(isOutput === false) {
      return(
        <a href={`#id=${id}`} onClick={e => this.handleInputZipDownload(e, id)}>all files (zip) <span className="fa fa-download"/></a>
      );
    }
    else {
      return(<span className="small-info-text">[ no files ]</span>);
    }
  }

  renderFileList(isOutput, fileList, chainId, collapsed) {
    if(_.isEmpty(fileList) || _.isNull(fileList)) {
      return(<span/>);
    }
    return(
      <ul className="file-list">
        <SmoothCollapse expanded={!collapsed}>
          { fileList.map((file) =>
            <li className="file-list-item monospace" key={file.path}>
              <span className="fa fa-hand-o-right small-info" />
              &nbsp;
              { this.renderFileLink(file.path, isOutput, chainId) } <span className="small-info-text">({ file.size })</span>
            </li>)
          }
        </SmoothCollapse>
      </ul>
    );
  }

  renderOutputLink(processChain) {
    if(!_.isNil(processChain.finished_at)) {
      return( this.renderChainZipLink(true, processChain.id) );
    }
    return(<span className="small-info">&nbsp;[ Not available ]</span>);
  }

  listExpandCollapseToggle(processChain, isInput) {
    if(isInput == true) {
      return(
        <a href="#" onClick={e => this.handleToggle(e, isInput, processChain.input_file_list_collapse, processChain.id, processChain.recipe_id)}>
          <span className={this.listIcon(processChain.input_file_list_collapse)}/>
          &nbsp;Input Files ({_.size(processChain.input_file_manifest)})
        </a>
      );
    } else {
      return(
        <a href="#" onClick={e => this.handleToggle(e, isInput, processChain.output_file_list_collapse, processChain.id, processChain.recipe_id)}>
          <span className={this.listIcon(processChain.output_file_list_collapse)}/>
          &nbsp;Output Files ({_.size(processChain.output_file_manifest)})
        </a>
      );
    }
  }

  listIcon(collapsed) {
    if(collapsed == true || _.isNil(collapsed)) {
      return "fa fa-caret-right";
    } else {
      return "fa fa-caret-down";
    }
  }

  render() {
    const { processChain } = this.props;
    return(
      <div>
        <div>
          {this.listExpandCollapseToggle(processChain, true)} <span className="silver">|</span> { this.renderChainZipLink(false, processChain.id) }
        </div>
        <div>{ this.renderFileList(false, processChain.input_file_manifest, processChain.id, processChain.input_file_list_collapse) }</div>
        <div>
          {this.listExpandCollapseToggle(processChain, false)} | { this.renderOutputLink(processChain) }
        </div>
        <div>{ this.renderFileList(true, processChain.output_file_manifest, processChain.id, processChain.output_file_list_collapse) }</div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { appState: state.appState };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch),
    dispatch: dispatch
  };
}

ExecutionFileList.propTypes = {
  processChain: PropTypes.object.isRequired,
  appState: PropTypes.object,
  dispatch: PropTypes.func
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExecutionFileList);
