import React, {PropTypes} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../../actions/authenticationActions';
import { setParameterTabView } from '../../actions/viewActions';
import { getPresetsForRecipeStep, setPresetForRecipeStepExecution } from '../../actions/recipeActions';

import * as viewConstants from '../../constants/Views';
import _ from 'lodash';
import Select from 'react-select';

import { Field, reduxForm } from 'redux-form';
import ParameterArrayForm from './ParameterArrayForm';

export class ParameterFormPosition extends React.Component {

  handlePopulatePreset = (e, preset) => {
    e.preventDefault();
    const { appState, dispatch, recipeStep } = this.props;

    console.log("POPULATING", preset)
  }

  // componentDidMount() {
  //   const { dispatch, appState, recipeStep } = this.props;
  //   dispatch(getPresetsForRecipeStep(recipeStep, appState));
  // }

  getPresets = (input) => {
    const { dispatch, appState, recipeStep } = this.props;
    dispatch(getPresetsForRecipeStep(recipeStep, appState));
    return(recipeStep.presets);
  }

  handleTabSwitch = (e, viewName) => {
    e.preventDefault();
    this.props.dispatch(setParameterTabView(viewName, this.props.recipeStep));
  }

  handleSubmit = (values) => {
    console.log("handleSubmit VALUES", values)
  }

  onChange = (value) => {
    const {recipeStep, dispatch} = this.props;
    dispatch(setPresetForRecipeStepExecution(value, recipeStep));
  }

  renderPresetParameter(value, key) {
    return(
      <div key={key}>
        <span className="fa fa-key"/> <b>{key}</b> <span className="fa fa-sliders"/> {value}
      </div>);
  }

  renderDescription(description) {
    if(_.isEmpty(description)) {
      return(null);
    }
    else {
      return(<div className="small-info">{description}</div>);
    }
  }

  renderSelectedPreset(preset) {
    if(_.isUndefined(preset) || _.isNull(preset)) {
      return(<div className="small-info">None selected</div>);
    }
    return(
      <div>
        { this.renderDescription(preset.description) }
        { _.keys(preset.execution_parameters).length } entries
        {_.map(preset.execution_parameters, (value, key) => this.renderPresetParameter(value, key) )}
      </div>
    );
  }

  renderPresetDropdown() {
    let { recipeStep } = this.props;

    return(
      <div>
        <Select value={recipeStep.selectedPreset} onChange={this.onChange} valueKey="id" labelKey="name" options={recipeStep.recipe_step_presets} />
        {this.renderSelectedPreset(recipeStep.selectedPreset)}
      </div>
    );
  }

  parametersToValues() {
    const { recipeStep} = this.props;

    let parameters = {}
    let valuePairs = [];
    Object.keys(recipeStep.execution_parameters).map(function(key, idx){
      let param = {};
      param.key = key;
      param.value = recipeStep.execution_parameters[key];
      valuePairs.push(param);
    });

    parameters.parameters = valuePairs;
    return parameters;
  }

  renderTabs(recipeStep) {
    if(recipeStep.parameterView == viewConstants.SHOW_PRESET_VIEW) {
      return(
        <div className="tab-container">
          <div className="tab-item inactive-parameter-tab" onClick={e => this.handleTabSwitch(e, viewConstants.SHOW_PARAMETER_FORM)}>Parameters</div>
          <div className="tab-item active-parameter-tab">Presets</div>
          <div className="tab-item parameter-tab-spacer"></div>
        </div>
      );
    }
    else if(recipeStep.parameterView == viewConstants.SHOW_PARAMETER_FORM) {
      return(
        <div className="tab-container">
          <div className="tab-item active-parameter-tab">Parameters</div>
          <div className="tab-item inactive-parameter-tab" onClick={e => this.handleTabSwitch(e, viewConstants.SHOW_PRESET_VIEW)}>Presets</div>
          <div className="tab-item parameter-tab-spacer"></div>
        </div>
      );
    }
  }

  renderTabView(recipeStep) {
    let parametersVisibilityClass, presetVisibilityClass;
    if(recipeStep.parameterView == viewConstants.SHOW_PRESET_VIEW) {
      parametersVisibilityClass = "visible";
      presetVisibilityClass = "hidden";
    } else if(recipeStep.parameterView == viewConstants.SHOW_PARAMETER_FORM) {
      parametersVisibilityClass = "hidden";
      presetVisibilityClass = "visible";
    }

    return(
        <div>
          <div className={parametersVisibilityClass}>
            <div className="parameter-container">
              <div className="preset-dropdown-container">
                {this.renderPresetDropdown(recipeStep)}
              </div>
            </div>
          </div>
          <div className={presetVisibilityClass}>
            <div className="preset-container">
              <ParameterArrayForm recipeStep={recipeStep} onSubmit={this.handleSubmit} initialValues={this.parametersToValues()}/>
            </div>
          </div>
        </div>
    );
  }

  render() {
    const { appState, recipeStep } = this.props;
    return (
      <div className="execution-parameter-position-container">
        {this.renderTabs(recipeStep)}
        {this.renderTabView(recipeStep)}
      </div>
    );
  }
}

ParameterFormPosition.propTypes = {
  appState: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
  recipeStep: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return { appState: state.appState };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch),
    dispatch: dispatch
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ParameterFormPosition);
