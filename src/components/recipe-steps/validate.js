const validate = values => {
  const errors = {};
  const parametersArrayErrors = [];
  if(!values.parameters) {
    return errors;
  }
  values.parameters.forEach((parameter, parameterIndex) => {
    const parameterErrors = {};
    if (!parameter || !parameter.key) {
      parameterErrors.key = 'Required';
      parametersArrayErrors[parameterIndex] = parameterErrors;
    }
    else if (/\s/.test(parameter.key)) {
      parameterErrors.key = 'No spaces allowed';
      parametersArrayErrors[parameterIndex] = parameterErrors;
    }
    if (!parameter || !parameter.value) {
      parameterErrors.value = 'Required';
      parametersArrayErrors[parameterIndex] = parameterErrors;
    }
    else if (/\s/.test(parameter.value)) {
      parameterErrors.value = 'No spaces allowed';
      parametersArrayErrors[parameterIndex] = parameterErrors;
    }
  });
  if (parametersArrayErrors.length) {
    errors.parameters = parametersArrayErrors;
  }
  return errors;
};

export default validate;
