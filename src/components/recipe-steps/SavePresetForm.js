import { Field, FieldArray, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import React from 'react';

export class SavePresetForm extends React.Component {

  required = value => (value ? undefined : 'Required');

  errorClass (touched, error) {
    let newClass = "input--inline-underline";
    if(touched && error) {
      return(`input-error ${newClass}`);
    }
    return(newClass);
  }

  errorIcon (touched, error) {
    if(touched && error) {
      return(<span className="fa fa-exclamation-triangle red" title={error}/>);
    }
    return null;
  }

  renderField = ({ input, label, type, meta: { touched, error } }) =>
    <span>
      <input {...input} type={type} placeholder={label} className={this.errorClass(touched, error)} />
      {this.errorIcon(touched, error)}
    </span>

  presetSaveButton() {
    const{processStep} = this.props;
    if(processStep.savingPreset === true || processStep.presetSaved === true) {
      return(
        <button className="inline-action-button disabled" disabled>
          <span className="fa fa-save"/> <span>Saved</span>
        </button>
      );
    }
    return(
      <button className="inline-action-button" data-tooltip="Save new preset">
        <span className="fa fa-save"/> <span>New preset</span>
      </button>
    );
  }

  render() {
    const { handleSubmit, pristine, reset, submitting, invalid, initialValues, processStep } = this.props;
    return (
      <form onSubmit={handleSubmit}>
        <Field
          name={`recipe_step_preset.name`}
          type="text"
          label="name"
          validate={[this.required]}
          component={this.renderField}
        />
        <br/>
        <Field
          name={`recipe_step_preset.description`}
          type="text"
          label="description"
          component={this.renderField}
        />
        <br/>
        {this.presetSaveButton()}
      </form>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    appState: state.appState,
    form: `presetform_${props.processStep.id}`
  };
}

export default connect(mapStateToProps)(reduxForm({})(SavePresetForm));
