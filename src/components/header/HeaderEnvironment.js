import React, {PropTypes} from 'react';
import { PRODUCTION, DEMO } from '../../constants/Environments';

const HeaderEnvironment = (props) => {
  let env = process.env.STAGE;
  if(env == PRODUCTION) {
    return(null);
  }

  let message = `You are on ${env}`;
  if(env == DEMO) {
    message = "This site is for demo purposes only.";
  } else if(process.env.NODE_ENV == 'development') {
    message = "You are on DEVELOPMENT";
  }
  return(<div className="env-info-header">{message}</div>);
};

HeaderEnvironment.propTypes = {
};

export default HeaderEnvironment;
