import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions/authenticationActions';
import SignInForm from '../SignInForm';
import SignOutForm from '../SignOutForm';
import _ from 'lodash';
import { Link } from 'react-router';

export class HeaderAccount extends Component {

  renderAdminPanel(account) {
    if(!_.isNil(account) && account.admin === true) {
      return(
        <span><Link to="/admin/dashboard" href="#"><span className="fa fa-pie-chart"/> Admin panel</Link> | </span>
      );
    }
    return(null);
  }

  render() {
    let { appState } = this.props;
    let { session } = appState;

    if(_.isNil(session.account)) {
      return (
        <span><span className="italic">Welcome, guest</span> | <Link to="/signin" href="#">Sign In</Link></span>
      );
    } else {
      return (
        <span>
          { this.renderAdminPanel(session.account) }
          <span className="italic">
            Signed in as: <a href="#">{session.account.email}</a>
            {' '}
            <SignOutForm />
          </span>
        </span>
      );
    }
  }
}

HeaderAccount.propTypes = {
  appState: PropTypes.object.isRequired
};

export default HeaderAccount;
