import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { browserHistory } from 'react-router';

import StepClassForm from './StepClassForm';

import * as actions from '../actions/recipeActions';

export class NewRecipeForm extends Component {

  handleSubmission = (e) => {
    e.preventDefault();
    const formData = new FormData();
    const { dispatch, appState } = this.props;
    const { stepClassList } = appState;

    formData.append('recipe[name]', this.refs.recipe_name.value);
    formData.append('recipe[description]', this.refs.recipe_description.value);
    formData.append('recipe[public]', this.refs.recipe_public.checked);
    for (let i = 0; i < stepClassList.length; i++) {
      formData.append('recipe[steps][]', stepClassList[i]);
    }

    const { signedIn, authToken, tokenType, client, expiry, uid } = appState.session;
    dispatch(actions.createRecipe(formData, signedIn, authToken, tokenType, client, expiry, uid));
  }

  handleCancel = (e) => {
    e.preventDefault();
    browserHistory.push("/recipes/");
  }

  render() {
    const { appState } = this.props;
    const { stepClassList } = appState;

    return(
      <form ref="createRecipeForm">
        <div className="form-input-container">
          <div className="form-input-labels">
            <div className="form-label" title="required">* name</div>
            <div className="form-label">description</div>
            <div className="form-label">public?</div>
          </div>
          <div className="form-input-boxes">
            <input className="input input--single-line-box" type="text" label="Name" ref="recipe_name" id="recipe_name" />
            <input className="input input--multi-line-box" type="textarea" cols="20" rows="40" label="Description" ref="recipe_description" id="recipe_description" />
            <p className="small-info-text">(anyone can see and execute it)</p>
            <input type="checkbox" ref="recipe_public" id="recipe_public" defaultChecked="true"/>
          </div>
        </div>
        <div className="form-input-container">
          <div className="form-input-labels">
            <div className="form-label" title="required">* step classes</div>
          </div>
          <div className="form-input-boxes">
            <p className="small-info-text">Find and select step classes to add. sorry, you cannot reorder them yet.</p>
            <StepClassForm
              stepClassList={stepClassList}
            />
          </div>
        </div>
        <div className="submit-button-container">
          <button className="action-button" onClick={this.handleSubmission}>Save</button>
          <button className="action-button action-button--cancel" onClick={this.handleCancel}>Cancel</button>
        </div>
      </form>
    );
  }
}

NewRecipeForm.propTypes = {
  dispatch: PropTypes.func.isRequired,
  appState: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    appState: state.appState
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch),
    dispatch: dispatch
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewRecipeForm);
