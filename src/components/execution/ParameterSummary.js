import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import { formValueSelector } from 'redux-form';
import * as Views from '../../constants/Views';

class ParameterSummary extends React.Component {

  compileResults() {
    let { state, recipe } = this.props;
    let compiledResults = [];
    let wrapper = this;
    _.map(recipe.recipe_steps, function(step) {
      let newObj = {};
      let params = wrapper.getParams(step, state);
      newObj.position = step.position;
      newObj.step = step;
      newObj.parameters = params;
      compiledResults.push(newObj);
    });
    return compiledResults;
    // execution_parameters: { "1" => {data: { "animal" => "honey badger", "abc" => "2.5" }} },
  }

  showingPresetView(recipeStep) {
    return(recipeStep.parameterView == Views.SHOW_PRESET_VIEW);
  }

  convertParameters(params) {
    let newArray = [];
    _.forEach(params, function(value, key) {
      let newObj = {};
      newObj.key = key;
      newObj.value = value;
      newArray.push(newObj);
    });
    return newArray;
  }

  getParams(recipeStep, state) {
    if(this.showingPresetView(recipeStep)) {
      if(_.isEmpty(recipeStep.selectedPreset)){
        return {};
      }
      let thing = this.convertParameters(recipeStep.selectedPreset.execution_parameters);
      return thing;
    } else {
      return this.assembleParameters(recipeStep.position, state);
    }
  }

  assembleParameters(position, state) {
    const selector = formValueSelector(`executionParameterForm_${position}`);
    let params = selector(state, 'parameters');
    return params;
  }

  renderParameters(step, parameters) {
    if(this.showingPresetView(step) && _.isEmpty(step.selectedPreset)) {
      return(<div className="tiny-info left-indent">Choose or create a preset</div>);
    }
    else if(_.isEmpty(parameters)) {
      return(<div className="tiny-info left-indent">None</div>);
    }
    else {
      return(
        <div className="parameter-summary-position-container">{
          _.map(parameters, (param) =>
            this.renderParameter(param, step.position)
        )}</div>
      );
    }
  }

  renderParameter(param, position) {
    return(
      <div key={`${param.key}_${position}` || Date.now} className="slightly-smaller">
        <span className="fa fa-key"/> {param.key || <span className="small-info">&lt;no key&gt;</span>} <span className="fa fa-sliders"/> {param.value || <span className="small-info">&lt;no value&gt;</span>}
      </div>
    );
  }

  render() {
    let results = this.compileResults();
    return(
      <div>
        {_.map(results, (result) =>
          <div key={result.position}>
            <div className="blue slightly-bigger">{result.position}. {result.step.human_readable_name}</div>
            { this.renderParameters(result.step, result.parameters) }
          </div>
        ) }
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    appState: state.appState,
    state: state
  };
}

ParameterSummary.propTypes = {
  recipe: PropTypes.object.isRequired,
  state: PropTypes.object.isRequired
};

export default connect(
  mapStateToProps
)(ParameterSummary);
