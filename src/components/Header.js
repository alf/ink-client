import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import HeaderAccount from './HeaderAccount';
import HeaderEnvironment from './HeaderEnvironment';

export class Header extends Component {
  render() {
    return (
      <div>
        <HeaderEnvironment />
        <div className="main-nav">
          <Link to="/">
            <span className="logo logo__small main-nav__logo" alt="INK logo">INK</span>
          </Link>
          <HeaderAccount
            appState={this.props.appState}
          />
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { appState: state.appState };
}

Header.propTypes = {
  appState: PropTypes.object.isRequired
};

export default connect(
  mapStateToProps
)(Header);
