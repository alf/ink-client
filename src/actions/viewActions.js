import * as actions from '../constants/ActionTypes';
import * as viewConstants from '../constants/Views';


////////////////////////// show recipe ///////////////////////////

export function setView(viewName) {
	return {
		type: actions.SET_VIEW,
		viewName: viewName
	};
}

export function resetView() {
	return {
		type: actions.SET_VIEW,
		viewName: viewConstants.SHOW_RECIPE_STEP_VIEW
	};
}

//////////////////////// show param tab ///////////////////////////

export function setParameterTabView(viewName, recipeStep) {
	return {
		type: actions.SET_PARAMETER_TAB_VIEW,
		viewName: viewName,
		recipeStep: recipeStep
	};
}
