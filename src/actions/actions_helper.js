import * as types from '../constants/ActionTypes';

export function checkStatus(response, dispatch, signedIn) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  } else if(response.status == 401) {
    dispatch(resetAuth());
    let error = new Error("Unauthorised");
    error.response = ["Please sign in"];
    throw error;
  } else if(response.status == 413){
    let error = new Error("File too large");
    error.response = ["That file is too large. Try a smaller one or contact your INK administrator to increase the allowed file size."];
    throw error;
  }
  return;
}

export function resetAuth() {
  return {
    type: types.RESET_AUTH
  };
}

export function setAlert(message, alertType) {
	return {
		type: types.SET_ALERT,
		message: message,
    alertType: alertType
	};
}

export function resetAlerts(arrayTypes) {
	return {
		type: types.RESET_ALERT_TYPES,
    arrayTypes: arrayTypes
	};
}

export function updateAuthHeaders(response) {
  let authToken = response.headers.get('Access-Token');
  let tokenType = response.headers.get('Token-Type');
  let client = response.headers.get('Client');
  let expiry = response.headers.get('Expiry');
  let uid = response.headers.get('uid');

  return {
    type: types.UPDATE_AUTH_HEADERS,
    authToken: authToken,
    expiry: expiry,
    tokenType: tokenType,
    client: client,
    uid: uid
  };
}
