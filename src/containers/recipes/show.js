import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { browserHistory } from 'react-router';
import { Link } from 'react-router';

import { subscribe, unsubscribe } from 'pusher-redux';
import * as eventActions from '../../constants/PusherActions';
import * as viewConstants from '../../constants/Views';

import * as actions from '../../actions/recipeActions';
import _ from 'lodash';
import { getRecipeWithTokens, resetPlaceholders } from '../../actions/recipeActions.js';
import { setView } from '../../actions/viewActions.js';
import { setAlert } from '../../actions/actions_helper';

import Header from '../../components/header/Header';
import AlertList from '../../components/AlertList';
import ExecutionFileForm from '../../components/ExecutionFileForm';
import RecipeStepDetail from '../../components/RecipeStepDetail';
import ExecutionList from '../../components/ExecutionList';
import FavouriteWidget from '../../components/FavouriteWidget';
import ExecutionInProgressWidget from '../../components/ExecutionInProgressWidget';

class RecipeShowPage extends Component {
  static propTypes = {
    actions: PropTypes.object.isRequired,
    appState: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    params: PropTypes.object,
    recipe: PropTypes.object
  };

  constructor(props, context) {
    super(props, context);
    this.subscribe = this.subscribe.bind(this);
    this.unsubscribe = this.unsubscribe.bind(this);
   }

  subscribe() {
    subscribe(this.chain_creation_channel(), 'chain_created', eventActions.CHAIN_CREATED);
  }

  unsubscribe() {
    unsubscribe(this.chain_creation_channel(), 'chain_created', eventActions.CHAIN_CREATED);
  }

  chain_creation_channel() {
    let { appState } = this.props;
    return(`${appState.session.account.id}_process_chain_creation`);
  }

  componentWillMount = () => {
    let { dispatch, appState, recipe, params } = this.props;
    const { session } = appState;
    dispatch(resetPlaceholders());

    if(_.isNull(session.account)) {
      dispatch(setAlert("Please sign in"));
      browserHistory.push('/signin');
      return;
    }

    if(_.isNull(appState.recipes) || _.isEmpty(recipe)) {
      dispatch(getRecipeWithTokens(this.props.params.id, appState));
    }

    this.subscribe();
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  componentDidMount() {
    this.findRecipe();
  }

  handleDeleteRecipe = (e) => {
    e.preventDefault();
    const { dispatch, appState, recipe } = this.props;
    const { signedIn, authToken, tokenType, client, expiry, uid } = appState.session;
    dispatch(actions.deleteRecipe(this.props.params.id, signedIn, authToken, tokenType, client, expiry, uid));
  }

  handleTabSwitch = (e, viewName) => {
    e.preventDefault();
    this.props.dispatch(setView(viewName));
  }

  findRecipe() {
    let { appState, params, dispatch } = this.props;
    let recipeId = _.toNumber(this.props.params.id);
    if(appState.selectedRecipe && appState.selectedRecipe.id === recipeId)
      return(appState.selectedRecipe);

    let recipe = _.find(appState.recipes, _.matchesProperty('id', recipeId));
    if((!_.isNull(recipe) && !_.isUndefined(recipe)) && _.has(recipe, 'recipe_steps')) {
      return(recipe);
    }
  }

  renderEditLink(recipe) {
    const { session } = this.props.appState;
    if(recipe.account_id == session.account.id) {
      return(
        <Link to={`/recipes/${recipe.id}/edit`}><span className="fa fa-pencil"/> edit this recipe</Link>
      );
    }
  }

  renderDeleteLink(recipe) {
    const { session } = this.props.appState;
    if(session.account.admin == true) {
      return(
        <div>
          <span>Delete?</span><br/>
          <a href="#" onClick={this.handleDeleteRecipe} className="delete-link"><span className="fa fa-times"/> destroy this recipe</a>
        </div>
      );
    }
  }

  currentRecipeLink(recipe) {
    return(`/recipes/${recipe.id}`);
  }

  renderExecutionList(recipe) {
    let { appState } = this.props;
    if(_.isEmpty(recipe.process_chains) && appState.executionPlaceholderCount == 0) {
      return(<p>This recipe has not been executed by you yet.</p>);
    }
    let { session } = this.props.appState;
    if(recipe){
      return(
        <ExecutionList
          recipe={recipe}
          session={session}
          executionPlaceholderCount = {appState.executionPlaceholderCount}
        />
      );
    } else {
      return(<span/>);
    }
  }

  renderRecipeSteps(recipeSteps) {
    if(_.isEmpty(recipeSteps)) {
      return(<p>No steps! This recipe is not very useful.</p>);
    }
    else {
      return(
        recipeSteps.map(recipeStep =>
          <RecipeStepDetail
            key={recipeStep.id}
            recipeStep={recipeStep}
          />)
      );
    }
  }

  renderTabs(recipe, appState) {
    if(appState.currentShowRecipeView == viewConstants.SHOW_RECIPE_STEP_VIEW) {
      return(
        <div className="tab-container">
          <div className="tab-item active-tab">Steps</div>
          <div className="tab-item inactive-tab" onClick={e => this.handleTabSwitch(e, viewConstants.SHOW_RECIPE_HISTORY_VIEW)}><ExecutionInProgressWidget recipe={recipe}/> Processes</div>
          <div className="tab-item tab-spacer"></div>
        </div>
      );
    }
    else if(appState.currentShowRecipeView == viewConstants.SHOW_RECIPE_HISTORY_VIEW) {
      return(
        <div className="tab-container">
          <div className="tab-item inactive-tab" onClick={e => this.handleTabSwitch(e, viewConstants.SHOW_RECIPE_STEP_VIEW)}>Steps</div>
          <div className="tab-item active-tab">Processes ({recipe.process_chains.length + appState.executionPlaceholderCount})</div>
          <div className="tab-item tab-spacer"></div>
        </div>
      );
    }
  }

  renderTabView(recipe, appState) {
    let stepsVisibilityClass, historyVisibilityClass;
    if(appState.currentShowRecipeView == viewConstants.SHOW_RECIPE_STEP_VIEW) {
      stepsVisibilityClass = "visible";
      historyVisibilityClass = "hidden";
    } else if(appState.currentShowRecipeView == viewConstants.SHOW_RECIPE_HISTORY_VIEW) {
      stepsVisibilityClass = "hidden";
      historyVisibilityClass = "visible";
    }

    return(
        <div>
          <div className={stepsVisibilityClass}>
            <div className="steps-container">
              {this.renderRecipeSteps(recipe.recipe_steps)}
            </div>
            <div>
              {this.renderDeleteLink(recipe)}
            </div>
          </div>
          <div className={historyVisibilityClass}>
            <div className="recipe-execution-history-container">
              {this.renderExecutionList(recipe)}
            </div>
          </div>
        </div>
    );
  }

  content(recipe, appState) {
    if(appState.getRecipeInProgress) {// if(_.isEmpty(appState.recipes)) {
      return(
        <div className="loading-centered">
          <div><span className="fa fa-gear fa-spin fa-3x fa-fw" /></div>
          <div><span className="small-info">Loading...</span></div>
        </div>
      );
    }

    if (_.isEmpty(recipe)) {
      return(
        <div className="loading-centered">
          <div><span className="fa fa-question fa-spin fa-3x fa-fw" /></div>
          <div><span className="small-info">I think that recipe might not exist...</span></div>
          <div><Link to="/recipes">back to recipe list</Link></div>
        </div>
      );
    }

    return (
      <div className="content-container">
        <div className="breadcrumb-container">
          <Link to="/" className="breadcrumb">Home</Link>
          <span className="breadcrumb-divider">&gt;&gt;</span>
          <Link to="/recipes" className="breadcrumb"><span className="fa fa-calendar-o"/> Recipes</Link>
          <span className="breadcrumb-divider">&gt;&gt;</span>
          <Link to={this.currentRecipeLink(recipe)} className="breadcrumb">{recipe.name}</Link>
        </div>

        <AlertList
          alerts={appState.alerts} />

        <div className="recipe-detail-view">
          <h1>Recipe: {recipe.name} <FavouriteWidget recipe={recipe} /></h1>
          <h4>{recipe.description}</h4>
          {this.renderEditLink(recipe)}

          <ExecutionFileForm recipe={recipe} />

          <div className="recipe-detail-view-container">
            {this.renderTabs(recipe, appState)}
            {this.renderTabView(recipe, appState)}
          </div>
        </div>
      </div>
    );
  }

  render() {
    const { dispatch, appState } = this.props;
    let recipe = this.findRecipe();

    return (
      <div>
        <Header
          appState={appState}
        />
        {this.content(recipe, appState)}
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    appState: state.appState
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch),
    dispatch: dispatch
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RecipeShowPage);
