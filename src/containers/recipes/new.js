import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { browserHistory } from 'react-router';
import { Link } from 'react-router';

import * as actions from '../../actions/recipeActions';
import _ from 'lodash';

import Header from '../../components/header/Header';
import NewRecipeForm from '../../components/NewRecipeForm';
import AlertList from '../../components/AlertList';
import { getAllRecipes } from '../../actions/recipeActions.js';
import { setAlert } from '../../actions/actions_helper';

class NewRecipePage extends Component {
  static propTypes = {
    actions: PropTypes.object.isRequired,
    appState: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    params: PropTypes.object
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount = () => {
    let { appState } = this.props;
    const { session, dispatch } = appState;

    if(_.isNull(session.account)) {
      dispatch(setAlert("Please sign in"));
      browserHistory.push('/signin');
      return;
    }
  }

  content(appState) {
    return (
      <div className="content-container">
        <div className="breadcrumb-container">
          <Link to="/" className="breadcrumb">Home</Link>
          <span className="breadcrumb-divider">&gt;&gt;</span>
          <Link to="/recipes" className="breadcrumb"><span className="fa fa-calendar-o"/> Recipes</Link>
          <span className="breadcrumb-divider">&gt;&gt;</span>
          <span className="breadcrumb">New recipe</span>
        </div>

        <AlertList
          alerts={appState.alerts} />

        <div className="recipe-detail-view">
          <h1>New Recipe</h1>

          <NewRecipeForm />
        </div>
      </div>
    );
  }

  render() {
    const { dispatch, appState } = this.props;
    const { session } = this.props.appState;

    return (
      <div>
        <Header
          appState={appState}
        />
        {this.content(appState)}
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    appState: state.appState
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch),
    dispatch: dispatch
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewRecipePage);
