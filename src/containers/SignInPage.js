import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { browserHistory } from 'react-router';

import * as actions from '../actions/authenticationActions';

import SignInForm from '../components/SignInForm';
import AlertList from '../components/AlertList';
import HeaderEnvironment from '../components/header/HeaderEnvironment';

class SignInPage extends Component {
  static propTypes = {
    actions: PropTypes.object.isRequired,
    appState: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired
  };

  goHome = (e) => {
    browserHistory.push('/');
  }

  render() {
    const { dispatch, appState } = this.props;
    return (
      <div>
        <HeaderEnvironment />
        <div className="vertical-center">
          <div className="horizontal-center">
            <div className="sign-in-logo-container">
              <span className="logo logo__large">INK</span>
              <span className="secondary-text">the converting framework</span>
            </div>
            <hr className="hr hr__sign-in"/>
            <AlertList
              alerts={appState.alerts}
              pageName="SignInPage" />
            <SignInForm appState={appState} dispatch={dispatch}/>
          </div>
        </div>
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    appState: state.appState,
    alerts: PropTypes.array.isRequired,
    recipes: PropTypes.array.isRequired
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch),
    dispatch: dispatch
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignInPage);
