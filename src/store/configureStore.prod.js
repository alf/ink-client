// import { createStore, applyMiddleware } from 'redux';
// import rootReducer from '../reducers';
//
// export default function configureStore(initialState) {
//   return createStore(rootReducer, initialState);
// }


import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from '../reducers';
import { setupPusher } from '../pusher.js';

export default function configureStore(initialState) {
  // return createStore(rootReducer, initialState);

  let store = createStore(rootReducer, initialState, compose(
    // Add other middleware on this line...
    applyMiddleware(thunk),
    )
  );

  setupPusher(store);

  return store;
}
