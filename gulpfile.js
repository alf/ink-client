var gulp = require('gulp');
var gutil = require('gulp-util');
var argv = require('minimist')(process.argv);
var gulpif = require('gulp-if');
var prompt = require('gulp-prompt');
var rsync = require('gulp-rsync');

gulp.task('deploy', function() {

  // Dirs and Files to sync
  //rsyncPaths = [path.dist, './src/fonts/*' ];
  rsyncPaths = ['/dist', './src/fonts/*' ];

  // Default options for rsync
  rsyncConf = {
    progress: true,
    incremental: true,
    relative: true,
    emptyDirectories: true,
    recursive: true,
    clean: true,
    exclude: [],
  };

  // Staging
  if (argv.staging) {

    rsyncConf.hostname = '162.243.148.158'; // hostname
    rsyncConf.username = 'admin'; // ssh username
    rsyncConf.destination = '~/ink-client/staging/dist'; // path where uploaded files go

  // Production
  } else if (argv.production) {

    rsyncConf.hostname = '162.243.148.158'; // hostname
    rsyncConf.username = 'admin'; // ssh username
    rsyncConf.destination = '~/ink-client/production/dist'; // path where uploaded files go

  } else if (argv.demo) {

    rsyncConf.hostname = '162.243.148.158'; // hostname
    rsyncConf.username = 'admin'; // ssh username
    rsyncConf.destination = '~/ink-client/demo/dist'; // path where uploaded files go

  // Missing/Invalid Target
  } else {
    throwError('deploy', gutil.colors.red('Missing or invalid target'));
  }


  // Use gulp-rsync to sync the files
  return gulp.src(rsyncPaths)
  .pipe(gulpif(
      argv.production,
      prompt.confirm({
        message: 'Heads Up! Are you SURE you want to push to PRODUCTION?',
        default: false
      })
  ))
  .pipe(rsync(rsyncConf));

});


function throwError(taskName, msg) {
  throw new gutil.PluginError({
      plugin: taskName,
      message: msg
    });
}
